﻿using UnityEngine;
using System.Collections;

public class CoinPickup : MonoBehaviour {

	public int pointsToAdd;

	public AudioClip coinSound1;
	public AudioClip coinSound2;
	
	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.GetComponent<PlayerController> () == null)
			return;
		
		ScoreManager.AddPoints (pointsToAdd);
		
		Destroy (gameObject);

		SoundController.Instance.PlaySingle (coinSound1, coinSound2);
	}
}
