﻿using UnityEngine;
using System.Collections;

public class VsPinkEnemy : MonoBehaviour {

	public LevelManager levelManager;
	public AudioClip hurtSound1;
	public AudioClip hurtSound2;
	
	// Use this for initialization
	void Start () {
		levelManager = FindObjectOfType<LevelManager> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.name == "Player") 
		{
			SoundController.Instance.PlaySingle(hurtSound1, hurtSound2);
		}
	}
}
