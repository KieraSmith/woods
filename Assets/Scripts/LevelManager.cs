﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

	public GameObject currentCheckpoint;

	private PlayerController player;

	public int pointPenaltyOnDeath;

	public HealthManager healthManager;

	public int decrease = -1;

	// Use this for initialization
	void Start () {
		player = FindObjectOfType<PlayerController> ();
		healthManager = FindObjectOfType<HealthManager>();
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void RespawnPlayer() 
	{
		//player.transform.position = currentCheckpoint.transform.position;
		player.transform.position = currentCheckpoint.transform.position;
		ScoreManager.AddPoints (-pointPenaltyOnDeath);
		healthManager.isDead = false;
	}


}
