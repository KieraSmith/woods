﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

	public string startLevel;
	public string worldMap;

	// Use this for initialization
	public void NewGame () {
		Application.LoadLevel (startLevel);
	}
	
	// Update is called once per frame
	public void LevelSelect () {
		Application.LoadLevel (worldMap);
	}

	public void QuitGame () {
		Application.Quit();
	}
}
